export const options = [{
    label: '联盟协作组',
    props: 'allianceName',
    width: 'auto',
    align: 'center'
  },
  {
    label: '证书联盟牵头单位',
    props: 'allianceSchool',
    width: 'auto',
    align: 'center'
  },
  {
    label: '联系人',
    props: 'allianceUser',
    width: 'auto',
    align: 'center'
  },
  {
    label: '联系电话',
    props: 'alliancePhone',
    width: 'auto',
    align: 'center'
  },
  {
    label: '电子邮箱',
    props: 'allianceEmail',
    width: 'auto',
    align: 'center',
    show: true
  }, {
    label: '备注',
    props: 'remark',
    width: 'auto',
    align: 'center',
    show: true
  },
  {
    label: '操作',
    props: 'actions',
    width: '150',
    align: 'center',
    fixed: 'right',
  }
]