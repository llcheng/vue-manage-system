export const options = [{
    label: '证书名称',
    props: 'certName',
    width: 'auto',
    align: 'center'
  },
  {
    label: '联盟名称',
    props: 'allianceName',
    width: 'auto',
    align: 'center'
  },
  {
    label: '联盟牵头单位',
    props: 'allianceSchool',
    width: 'auto',
    align: 'center'
  },{
        label: '操作',
        props: 'actions',
        width: '150',
        align: 'center',
        fixed: 'right',
    }
]