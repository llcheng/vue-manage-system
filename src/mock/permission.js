// 引入mockjs
import Mock from "mockjs";
import { options } from "../views/home/options";
import axios from "axios";
const Random = Mock.Random;

let menuList = [
  {
    title: "首页",
    url: "/home",
    icon: "HomeFilled",
  },
  {
    title: "源数据管理",
    url: "/page",
    icon: "Checked",
    children: [
      {
        title: "数据同步",
        url: "/form",
        icon: "RefreshRight",
        children: [
          {
            title: "试点院校信息",
            url: "/baseForm",
            //url:"/fileImport",
            icon: "Menu",
          },
          {
            url: "/stepFrom",
            title: "试点院校证书申报",
            icon: "Menu",
          },
          {
            url: "/advancedForm",
            title: "证书考核详情",
            icon: "Menu",
          },
        ],
      },
      {
        url: "/system", //llcheng title
        title: "数据备份",
        icon: "Lock",
        children: [
          {
            url: "/Backup",
            title: "一键导出备份",
            icon: "Menu",
          },
            /*
          {
            url: "/UserList",
            title: "内嵌表格",
            icon: "Menu",
          },
          {
            url: "/RoleList",
            title: "滑动加载",
            icon: "Menu",
          },
          {
            url: "/MenuList",
            title: "可编辑Table",
            icon: "Menu",
          },
          {
            url: "/importExcel",
            title: "导入Excel",
            icon: "Menu",
          },

             */
        ],
      },
 /*
      {
        url: "/goods",
        title: "列表页",
        icon: "List",
        children: [
          {
            url: "/goodCategory",
            title: "基础列表",
            icon: "Menu",
          },
          {
            url: "/cardList",
            title: "卡片列表",
            icon: "Menu",
          },
          {
            url: "/searchList",
            title: "搜索列表",
            icon: "Menu",
          },
        ],
      },*/
        /*
      {
        url: "/ErrorMessage",
        title: "异常页面",
        icon: "WarningFilled",
        children: [
          {
            url: "/404",
            title: "404",
            icon: "Menu",
          },
          {
            url: "/500",
            title: "500",
            icon: "Menu",
          },
        ],
      },*/
    ],
  },

  {
    title: "角色管理",
    url: "/echarts",
    //icon: "Histogram",
    icon: "Avatar",
    children: [
      {
        title: "权限配置",
        url: "/role",
        icon: "Menu",
      },
      {
        title: "用户配置",
        url: "/userRole",
        icon: "Menu",
      },
        /*
      {
        title: "地图",
        url: "/echarts/map",
        icon: "Menu",
        children: [
          {
            url: "/baidumap",
            title: "百度地图",
            icon: "Menu",
          },
          // {
          //   url: "/gaodemap",
          //   title: "高德地图",
          //   icon: "Menu",
          // },
        ],
      },*/

        /*
      {
        title: "雷达图",
        url: "/radar",
        icon: "Menu",
      },
      {
        title: "柱状图",
        url: "/histogram",
        icon: "Menu",
      },
      {
        title: "折线图",
        url: "/line",
        icon: "Menu",
      },

         */
    ],
  },

  {
    url: "/able",
    title: "数据分析",
    icon: "Histogram",
    children: [
      {
        url: "/certificateList",
        title: "X证书申报列表",
        icon: "Menu",
      },{
        url: "/xAnalysis1",
        title: "分析报表一",
        icon: "Menu",
      },{
        url: "/xAnalysis2",
        title: "分析报表二",
        icon: "Menu",
      },{
        url: "/xAnalysis3",
        title: "分析报表三",
        icon: "Menu",
      },{
        url: "/xAnalysis4",
        title: "分析报表四",
        icon: "Menu",
      },
        /*
        {
        url: "/watermark",
        title: "水印",
        icon: "Menu",
      },
      {
        url: "/countTo",
        title: "数字动画",
        icon: "Menu",
      },
      {
        url: "/batchImport",
        title: "图片上传",
        icon: "Menu",
      },
      {
        url: "/markmap",
        title: "思维导图",
        icon: "Menu",
      },
      {
        url: "/jsmind",
        title: "可编辑的思维导图",
        icon: "Menu",
      },
      {
        url: "/fileImport",
        title: "文件上传",
        icon: "Menu",
      },
      {
        url: "/Filepreview",
        title: "文件预览",
        icon: "Menu",
      },
      {
        url: "markdown",
        icon: "Platform",
        title: "编辑器",
        children: [
          {
            url: "/wangEditor",
            title: "富文本编辑器",
            icon: "Menu",
          },
          {
            url: "/markdown",
            title: "markdown",
            icon: "Menu",
          },
        ],
      },
      {
        url: "/strength",
        title: "密码强度",
        icon: "Menu",
      },
      {
        url: "/validation",
        title: "验证组件",
        icon: "Menu",
      },
      {
        url: "/guide",
        title: "引导页",
        icon: "Menu",
      },
      {
        url: "/embedded",
        title: "内嵌页",
        icon: "Menu",
      },

         */
    ],
  },
  {
    url: "/directives",
    title: "联盟协作组管理",
    icon: "HelpFilled",
    children: [
      {
        url: "/Department",
        title: "联盟协作组列表",
        icon: "Menu",
      },{
        url: "/allianceCert",
        title: "联盟协作组证书列表",
        icon: "Menu",
      },
      /*
      {
        url: "/copy",
        title: "复制",
        icon: "Menu",
      },
      {
        url: "/Drag",
        title: "拖拽",
        icon: "Menu",
      },
      {
        url: "/debounceDirect",
        title: "防抖指令",
        icon: "Menu",
      },
      {
        url: "/throttle",
        title: "节流指令",
        icon: "Menu",
      },
      // {
      //   url: "/longPress",
      //   title: "长按指令",
      //   icon: "Menu",
      // },


       */
    ],
  },
  {
    url: "/flow",
    title: "周报管理",
    icon: "Message",
    children: [
      {
        url: "/uploadReportZip",
        title: "周报压缩包上传",
        icon: "Menu",
      },{
        url: "/reportList",
        title: "周报分析",
        icon: "Menu",
      },{
        url: "/reportTemplate",
        title: "周报模板",
        icon: "Menu",
      },
    ],
  },
  {
    url: "/video",
    title: "邮件管理",
    icon: "Promotion",
    //
    children: [
      {
        url: "/emailEdit",
        title: "自定义邮件",
        icon: "Menu",
      },
      {
        url: "/emailSend",
        title: "格式邮件",
        icon: "Menu",
      },
        /*
      {
        url: "/video",
        title: "视频播放器",
        icon: "Menu",
      },

         */
    ],
  },
  {
    url: "/DataReport",
    title: "日志管理",
    icon: "TrendCharts",

    children: [
      {
        url: "/logList",
        title: "日志列表",
        icon: "Menu",
      },
    ],
  },
  // {
  //   url: "/material",
  //   title: "素材中心",
  //   icon: "PictureFilled",
  //   children: [{
  //     url: "/materialIndex",
  //     title: "素材管理",
  //     icon: "Menu",
  //   }, ],
  // },

];

export const LoginInfo = (options) => {
  console.log(options, "接收post参数");
  const { username, password } = JSON.parse(options.body);
  if (username == "admin" && password != "123456") {
    return {
      code: "-200",
      data: {
        message: "用户不存在",
      },
    };
  } else {
    return {
      code: "200",
      data: {
        user_id: Random.id(),
        name: Random.cname(),
        token: Random.guid(),
        image:
          "https://img2.baidu.com/it/u=2859542338,3761174075&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1660064400&t=6fe6057370cbe369654ff2e132d02a37",
      },
    };
  }
};

export const getMenuList = (options) => {
  const obj = JSON.parse(options.body);
  return {
    code: 200,
    data: {
      menuList: menuList,
    },
  };
};

// 用户列表
let userList = [];
for (let index = 0; index < 50; index++) {
  let obj = {
    id: Random.id(),
    username: Random.cname(),
    email: Random.email(),
    date: Random.date(),
    address: Random.city(true),
    content: Random.csentence(),
  };
  userList.push(obj);
}
const param2Obj = (url) => {
  let obj = JSON.parse(url);
  let page = obj.page;
  console.log(page);
  return page;
};

export const UserList = (options) => {
  const currentPage = param2Obj(options.body);
  let cameraData = userList.filter((item, index) => {
    return index >= (currentPage - 1) * 10 && index < currentPage * 10;
  });
  return {
    code: 200,
    data: {
      total: userList.length,
      userList: cameraData,
    },
  };
};

export const addUserList = (options) => {
  console.log("传过来的数据" + JSON.parse(options.body));
  let obj = JSON.parse(options.body);
  obj.id = Random.id();
  userList.unshift(obj); // 将前台返回来的数据，拼接到数组中。
  return {
    data: userList,
    id: obj.id,
  };
};

// 数据的修改操作
export const listUpdate = (options) => {
  let obj = JSON.parse(options.body);
  userList = userList.map((val) => {
    return val.id == obj.id ? obj : val;
  });
  return {
    data: userList,
  };
};

export const Newslist = (options) => {
  let obj = JSON.parse(options.body);
  let newList = [];
  for (let i = 0; i < 10; i++) {
    let item = {
      title: Random.csentence(5, 8), //  Random.csentence( min, max )
      notifyPic: Random.dataImage("300x250", "mock的图片"), // Random.dataImage( size, text ) 生成一段随机的 Base64 图片编码
      notifyType: Random.integer(1, 3), //随机生成1-3的Integer
      isTop: Random.integer(1, 2), //随机生成1-2的Integer
      createUser: Random.cname(), // Random.cname() 随机生成一个常见的中文姓名
      email: Random.email(),
      number: "$" + Random.integer(100, 5000) + ".00",
      createTime: Random.date() + " " + Random.time(),
      pay_state: "100" + Random.integer(1, 3),
    };
    newList.push(item);
  }
  return {
    data: newList,
  };
};
export const orderLists = (options) => {
  let obj = JSON.parse(options.body);
  let orderList = [];
  for (let i = 0; i < 60; i++) {
    let item = {
      goodsId: i,
      code: Random.guid(),
      title: Random.ctitle(4, 5),
      commodity: "10010" + i,
      goodsname: Random.cword(2, 6), //  Random.csentence( min, max )
      brand: Random.cword(2, 4),
      sku: "1ml", // Random.dataImage( size, text ) 生成一段随机的 Base64 图片编码
      inventory: Random.integer(10, 100),
      number: Random.integer(100, 5000), //100到5000的随机整数
      costPrice: Random.integer(10, 20),
      amount: Random.integer(100, 200), //100到5000的随机整数
      itemEdit: false,
      ItemData: [
        {
          address: Random.city(true),
          email: Random.email(),
          state: Random.boolean(),
          salenumber: Random.integer(10, 100),
          receivable: Random.integer(10, 100),
        },
      ],
    };
    orderList.push(item);
  }

  return {
    total: 60,
    data: obj.size == 10 ? orderList.slice(obj.size * obj.page - obj.size, obj.size * obj.page) : orderList,
  };
};


export const homeList = (options) => {
  let homeList = [];

    let obj1 = {
      certName: "WPS办公应用职业技能等级证书",
      stuNum: "8892",
      schoolNum: "66",
      examStuNum: "3774",
      failStuNum: "3058",
      passRate: "81.03%",
    };
    homeList.push(obj1);
  let obj2 = {
    certName: "幼儿照护职业技能等级证书",
    stuNum: "7053",
    schoolNum: "78",
    examStuNum: "4312",
    failStuNum: "1481",
    passRate: "34.35%",
  };
  homeList.push(obj2);
  let obj3 = {
    certName: "网店运营推广职业技能等级证书",
    stuNum: "3700",
    schoolNum: "53",
    examStuNum: "2913",
    failStuNum: "1925",
    passRate: "66.08%",
  };
  homeList.push(obj3);
  let obj4 = {
    certName: "业财一体信息化应用职业技能等级证书",
    stuNum: "3674",
    schoolNum: "51",
    examStuNum: "3227",
    failStuNum: "2229",
    passRate: "69.07%",
  };
  homeList.push(obj4);
  let obj = {
    certName: "汽车运用与维修职业技能等级证书",
    stuNum: "2215",
    schoolNum: "58",
    examStuNum: "956",
    failStuNum: "569",
    passRate: "59.52%",
  };
  homeList.push(obj);
  let obj5 = {
    certName: "建筑信息模型（BIM）职业技能等级证书",
    stuNum: "2180",
    schoolNum: "34",
    examStuNum: "1571",
    failStuNum: "737",
    passRate: "46.91%",
  };
  homeList.push(obj5);
  let obj6 = {
    certName: "母婴护理职业技能等级证书",
    stuNum: "2132",
    schoolNum: "22",
    examStuNum: "1636",
    failStuNum: "773",
    passRate: "47.25%",
  };
  homeList.push(obj6);
  let obj7 = {
    certName: "物流管理职业技能等级证书",
    stuNum: "1776",
    schoolNum: "23",
    examStuNum: "1602",
    failStuNum: "898",
    passRate: "56.05%",
  };
  homeList.push(obj7);
  let obj8 = {
    certName: "生涯规划指导职业技能等级证书",
    stuNum: "1745",
    schoolNum: "9",
    examStuNum: "190",
    failStuNum: "165",
    passRate: "86.84%",
  };
  homeList.push(obj8);
  let obj9 = {
    certName: "老年照护职业技能等级证书",
    stuNum: "1628",
    schoolNum: "23",
    examStuNum: "1230",
    failStuNum: "501",
    passRate: "40.73%",
  };
  homeList.push(obj9);





  return {
    data: homeList,
  };
};



export const noticeLists = (options) => {
  let noticeLists = [];

  for (let index = 0; index < 1; index++) {
    let item = {
      //text: Random.csentence(),
      text: '1+X数据分析系统上线',
    };
    noticeLists.push(item);
  }


  return {
    data: noticeLists,
  };
};

export const cardlists = (options) => {
  let obj = JSON.parse(options.body);
  let cardlists = [];
  for (let i = 0; i < 50; i++) {
    let item = {
      title: Random.csentence(5, 8), //  Random.csentence( min, max )
      user_name: Random.cname(), // Random.cname() 随机生成一个常见的中文姓名
      email: Random.email(),
      number: "$" + Random.integer(100, 5000) + ".00",
      date: Random.date(),
      pay_state: "100" + Random.integer(1, 3),
      content: Random.csentence(),
    };
    cardlists.push(item);
  }
  return {
    data: cardlists,
  };
};

const url = `/public/json/`;
let allianceUserList = [];
const response = await axios.get(url+'allianceUser.json'); // 根据实际情况修改路径
allianceUserList = response.data;

export const getAllianceUserList = (options) => {
  const currentPage = param2Obj(options.body);
  let cameraData = allianceUserList.filter((item, index) => {
    return index >= (currentPage - 1) * 10 && index < currentPage * 10;
  });
  return {
    code: 200,
    data: {
      total: allianceUserList.length,
      allianceUserList: cameraData,
    },
  };
};


let allianceCertList = [];
const allianceCertResponse = await axios.get(url+'allianceCert.json'); // 根据实际情况修改路径
allianceCertList = allianceCertResponse.data;
export const getAllianceCertList = (options) => {
  const currentPage = param2Obj(options.body);
  let cameraData = allianceCertList.filter((item, index) => {
    return index >= (currentPage - 1) * 10 && index < currentPage * 10;
  });
  return {
    code: 200,
    data: {
      total: allianceCertList.length,
      allianceCertList: cameraData,
    },
  };
};



