import router from "./router.js";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import store from "../store";
NProgress.configure({
  ease: "ease",
  speed: 500,
});

const writeNames = ["/login"];
router.beforeEach((to, from, next) => {
  NProgress.start();
  console.log(store.getters);
  if (sessionStorage.getItem("token")) {
    if (to.path === "/login") {
      next("/");
    }
    const permission = sessionStorage.getItem("permission")

    if (to.path === "/baseForm") {
      if(permission.includes('源数据管理')){
        return next();
      } else {
        next("/404");
      }
    }
    if (to.path === "/stepFrom") {
      if(permission.includes('源数据管理')){
        return next();
      } else {
        next("/404");
      }
    }
    if (to.path === "/advancedForm") {
      if(permission.includes('源数据管理')){
        return next();
      } else {
        next("/404");
      }
    }
    if (to.path === "/Backup") {
      if(permission.includes('源数据管理')){
        return next();
      } else {
        next("/404");
      }
    }
    if (to.path === "/role") {
      if(permission.includes('角色管理')){
        return next();
      } else {
        next("/404");
      }
    }
    if (to.path === "/userRole") {
      if(permission.includes('角色管理')){
        return next();
      } else {
        next("/404");
      }
    }

    return next();
  } else {
    if (writeNames.includes(to.path)) {
      next();
    } else {
      next("/login");
    }
  }
});

router.afterEach((transition) => {
  NProgress.done();
});

export default router;


function getCookieValue(name) {
  const cookies = document.cookie.split('; ');
  for (let i = 0; i < cookies.length; i++) {
    const [key, value] = cookies[i].split('=');
    if (key.trim() === name) {
      return value;
    }
  }
  return null; // 如果未找到对应的 cookie，则返回 null
}